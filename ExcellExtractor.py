
from openpyxl import load_workbook
import os


class ExcellExtractor():

    def __init__(self, row='B'):
        self.row = row

    def setRow(self, row):
        self.row = row

    def get_row_info(self, row):
        excell_path = 'excell/2852.xlsx'
        filepath = os.path.join(os.path.dirname(__file__), excell_path)
        wb = load_workbook(filepath)
        sheet = wb.active
        b1 = sheet[row]
        return b1.value



