import json

from flask import Flask, render_template, request
from flask_swagger_ui import get_swaggerui_blueprint
import os
from ExcellExtractor import ExcellExtractor
import time


app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        with open('file.json', 'w') as f:
            json.dump(request.form, f)
        os.system('python index.py')

        time.sleep(10)
        exll = ExcellExtractor()
        tasks_sexo = exll.get_row_info('B7')
        tasks_año = exll.get_row_info('B8')
        tasks_provincia = exll.get_row_info('A9')
        tasks_results = exll.get_row_info('B9')
        tasks = [tasks_sexo, tasks_año, tasks_provincia, tasks_results]
        return render_template('index.html', tasks=tasks)
    else:
        return render_template('index.html')

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Adrian-Python-Flask-REST"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)

