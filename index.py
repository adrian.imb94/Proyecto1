
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as E
from selenium.webdriver.common.by import By
import time
import os
import json
import shutil
from pathlib import Path

if Path('C:\\Users\\adrian.moral\\Downloads\\2852.xlsx').is_file():
    os.remove('C:\\Users\\adrian.moral\\Downloads\\2852.xlsx')

# Opciones de navegación
options = webdriver.ChromeOptions()
options.add_argument('--start-maximized')
options.add_argument('--disable-extensions')
#options.add_experimental_option('prefs', "C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\")

driver_path = 'C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\webdriver\\chromedriver.exe'

driver = webdriver.Chrome(driver_path, options=options)

# Iniciarla en la pantalla 2
driver.set_window_position(2000, 0)
driver.maximize_window()
time.sleep(2)

# Inicializamos el navegador
driver.get('https://www.ine.es/')

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/nav/div/a'))) \
    .click()

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/nav/div/nav/div[1]/div/ul[1]/li[2]/ul/li[2]/div[1]/a'))) \
    .click()

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/nav/div/nav/div[1]/div/ul[1]/li[2]/ul/li[2]/div[2]/div/ul/li[4]/div[1]/a'))) \
    .click()

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/nav/div/nav/div[1]/div/ul[1]/li[2]/ul/li[2]/div[2]/div/ul/li[4]/div[2]/div/ul/li[2]/a'))) \
    .click()
WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/div[2]/div[2]/ul/li[1]/div/ul/li/table/tbody/tr[1]/th/a'))) \
    .click()

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/section[2]/div/div/div[1]/ul/li/ul/li[1]/a'))) \
    .click()
WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/div[4]/div[2]/ol/li[1]/ol/li[1]/a[3]'))) \
    .click()
WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/form/ul/li[1]/ul/li[1]/div/fieldset/div[2]/button[2]'))) \
    .click()

##Recoger datos desde el json


with open('file.json') as json_file:
    data = json.load(json_file)

    data_provincia = data['i_provincia']
    data_sexo = data['i_sexo']
    data_año = data['i_ano']

el = driver.find_element_by_id('cri28369')
for option in el.find_elements_by_tag_name('option'):
    if option.text[3:] == data_provincia:
        option.click()
        break

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/form/ul/li[1]/ul/li[2]/div/fieldset/div[2]/button[2]'))) \
    .click()

el = driver.find_element_by_id('cri28370')
for option in el.find_elements_by_tag_name('option'):
    if option.text == data_sexo:
        option.click()
        break

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/form/ul/li[1]/ul/li[3]/div/fieldset/div[3]/button[2]'))) \
    .click()

el = driver.find_element_by_id('periodo')
for option in el.find_elements_by_tag_name('option'):
    if option.text == data_año:
        option.click()
        break

WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/form/div[5]/input[3]'))) \
    .click()


## Extracción de datos
WebDriverWait(driver, 10) \
    .until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                        '/html/body/div[1]/main/ul[1]/li/div/div/form[2]/button')))\
    .click()

waitc = WebDriverWait(driver, 10)
waitc.until(E.frame_to_be_available_and_switch_to_it('thickBoxINEfrm'))

WebDriverWait(driver, 10)
i = driver.find_element_by_xpath('/html/body/form/ul/li[3]/label/input')

driver.execute_script('arguments[0].click();', i)
driver.switch_to.default_content()


#Movemos la carpeta al contenedor de nuestro programa
if Path('C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\excell\\2852.xlsx').is_file():
    os.remove('C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\excell\\2852.xlsx')
    time.sleep(10)
    shutil.move('C:\\Users\\adrian.moral\\Downloads\\2852.xlsx', 'C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\excell')
else:
    time.sleep(10)
    shutil.move('C:\\Users\\adrian.moral\\Downloads\\2852.xlsx', 'C:\\Users\\adrian.moral\\Adrián\\RPA\\Proyecto1\\excell')

driver.stop_client()
driver.quit()
driver.close()
os.system('python app.py')



